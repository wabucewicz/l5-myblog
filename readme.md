# Laravel 5 Blog Application

## Installation

1. composer install
2. php artisan migrate --seed

## Default data

1. User name: admin
2. Email: admin@mail.com
3. Password: secret
