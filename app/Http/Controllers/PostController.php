<?php

namespace App\Http\Controllers;

use App\Post;

class PostController extends Controller
{
    public function getAllPosts()
    {
        $posts = Post::orderBy('created_at', 'desc')->paginate(6);
        foreach ($posts as $post) {
            if (strpos($post->content, '[-- READ MORE --]') !== false) {
                $tmp = explode('[-- READ MORE --]', $post->content);
                $post->lead = $tmp[0];
            } else {
                $post->lead = '';
            }
        }

        return view('home', ['posts' => $posts]);
    }

    public function getPost($id)
    {
        $post = Post::find($id);

        if (strpos($post->content, '[-- READ MORE --]') !== false) {
            $tmp = explode('[-- READ MORE --]', $post->content);
            $post->lead = $tmp[0];
            $post->content = $tmp[1];
        } else {
            $post->lead = '';
        }

        $comments = $post->comments;

        return view('post', ['post' => $post, 'comments' => $comments]);
    }
}
