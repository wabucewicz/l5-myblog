<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use App\Comment;
use Auth;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function saveNewComment($id, $slug, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'comment' => 'required|max:1000',
        ]);

        $attributeNames = array(
           'comment' => 'Comment',
        );

        $validator->setAttributeNames($attributeNames);

        if ($validator->fails()) {
            return redirect('/'.$id.'-'.$slug)
                ->withInput()
                ->withErrors($validator);
        }

        $comment = new Comment;
        $comment->comment = $request->comment;
        $comment->user_id = Auth::user()->id;
        $comment->post_id = $id;
        $comment->save();

        return redirect('/'.$id.'-'.$slug)->with('message', 'success|Comment added successfully.');
    }

}
