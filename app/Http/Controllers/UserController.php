<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use App\User;
use Hash;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getEditUser($id)
    {
        $user = User::find($id);

        return view('user', ['user' => $user]);
    }

    public function saveEditUser($id, Request $request)
    {
        $user = User::find($id);

        $rules = [
            'name' => 'required|max:100',
            'email' => 'required|email|max:100',
            'pass1' => 'min:6|max:100',
            'pass2' => 'max:100|same:pass1',
        ];

        $validator = Validator::make($request->all(), $rules);

        $attributeNames = array(
           'name' => 'Name',
           'email' => 'Email',
           'pass1' => 'Password',
           'pass2' => 'Repeat password'
        );

        $validator->setAttributeNames($attributeNames);

        if ($validator->fails()) {
            return redirect('/user/'.$id)
                ->withInput()
                ->withErrors($validator);
        }

        $user->name = $request->name;
        $user->email = $request->email;
        if ($request->pass1 != '') {
            $user->password = Hash::make($request->pass1);
        }

        $file = $request->file('avatar');

        if (isset($request->image_change) && $request->image_change == 1) {
            File::Delete(public_path('img/avatars/'.$user->avatar));
            $user->avatar = '';
        }

        if (isset($file)) {
            File::Delete('img/avatars/'.$user->avatar);
            $avatar_name = md5(microtime()).'.jpg';
            Image::make($file)->fit(150, 150)->save(public_path('img/avatars/'.$avatar_name))->encode('jpg');
            $user->avatar = $avatar_name;
        }

        $user->save();

        return redirect('/user/'.$id)->with('message', 'success|Profile data saved successfully.');
    }
}
