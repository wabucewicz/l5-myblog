<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Post;
use Illuminate\Support\Facades\Validator;
use Auth;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class PostController extends Controller
{
    public function getListOfPosts()
    {
        $posts = Post::all();

        return view('admin.posts.list', ['posts' => $posts]);
    }

    public function getEditPost($id)
    {
        $post = Post::find($id);

        return view('admin.posts.edit', ['post' => $post]);
    }

    public function saveEditPost($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:100',
            'slug' => 'required|max:50',
            'content' => 'required',
        ]);

        $attributeNames = array(
           'title' => 'Title',
           'slug' => 'Slug',
           'content' => 'Content',
        );

        $validator->setAttributeNames($attributeNames);

        if ($validator->fails()) {
            return redirect('/admin/posts/edit/'.$id)
                ->withInput()
                ->withErrors($validator);
        }

        $post = Post::find($id);

        $file = $request->file('cover');

        if (isset($request->image_change) && $request->image_change == 1) {

            File::Delete(public_path('img/covers/'.$post->cover));
            $post->cover = '';
        }
        if (isset($file)) {
            File::Delete('img/covers/'.$post->cover);
            $cover_name = md5(microtime()).'.jpg';
            Image::make($file)->fit(900, 300)->save(public_path('img/covers/'.$cover_name))->encode('jpg');
            $post->cover = $cover_name;
        }

        $post->title = $request->title;
        $post->slug = $request->slug;
        $post->content = $request->content;
        $post->save();

        return redirect('/admin/posts')->with('message', 'success|Post saved successfully.');
    }

    public function getNewPost()
    {
        return view('admin.posts.new');
    }

    public function saveNewPost(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:100',
            'slug' => 'required|max:50',
            'content' => 'required',
        ]);

        $attributeNames = array(
           'title' => 'Title',
           'slug' => 'Slug',
           'content' => 'Content',
        );

        $validator->setAttributeNames($attributeNames);

        if ($validator->fails()) {
            return redirect('/admin/posts/new')
                ->withInput()
                ->withErrors($validator);
        }

        $post = new Post();

        $file = $request->file('cover');

        if (isset($file)) {
            $cover_name = md5(microtime()).'.jpg';
            Image::make($file)->fit(900, 300)->save(public_path('img/covers/'.$cover_name))->encode('jpg');
            $post->cover = $cover_name;
        }

        $post->title = $request->title;
        $post->slug = $request->slug;
        $post->content = $request->content;
        $post->user_id = Auth::user()->id;
        $post->save();

        return redirect('/admin/posts')->with('message', 'success|Post created successfully.');
    }

    public function deletePost(Post $post)
    {
        $post->delete();

        return redirect('/admin/posts')->with('message', 'success|Post deleted successfully.');
    }
}
