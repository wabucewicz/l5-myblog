<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Comment;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    public function getListOfComments()
    {
        $comments = Comment::all();
        return view('admin.comments.list', ['comments' => $comments]);
    }

    public function getEditComment($id)
    {
        $comment = Comment::find($id);
        return view('admin.comments.edit', ['comment' => $comment]);
    }

    public function saveEditComment($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'comment' => 'required|max:1000',
        ]);

        $attributeNames = array(
           'comment' => 'Comment',
        );

        $validator->setAttributeNames($attributeNames);

        if ($validator->fails()) {
            return redirect('/admin/comments/edit/'.$id)
                ->withInput()
                ->withErrors($validator);
        }

        $comment = Comment::find($id);
        $comment->comment = $request->comment;
        $comment->save();

        return redirect('/admin/comments')->with('message', 'success|Comment saved successfully.');
    }

    public function deleteComment (Comment $comment)
    {
        $comment->delete();

        return redirect('/admin/comments')->with('message', 'success|Comment deleted successfully.');
    }
}
