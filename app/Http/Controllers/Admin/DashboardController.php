<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\User;
use App\Comment;
use App\Post;

class DashboardController extends Controller
{
    public function index()
    {
        $posts = Post::limit(10)->get();
        $comments = Comment::limit(10)->get();
        $users = User::limit(10)->get();

        $count = [];
        $count['posts'] = Post::count();
        $count['comments'] = Comment::count();
        $count['users'] = User::count();

        return view('admin.dashboard', ['count' => $count, 'posts' => $posts, 'comments' => $comments, 'users' => $users]);
    }
}
