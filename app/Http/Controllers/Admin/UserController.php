<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Hash;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class UserController extends Controller
{
    public function getListOfUsers()
    {
        $users = User::all();

        return view('admin.users.list', ['users' => $users]);
    }

    public function getNewUser()
    {
        return view('admin.users.new');
    }

    public function saveNewUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:100',
            'email' => 'required|email|max:100',
            'pass1' => 'required|min:6|max:100',
            'pass2' => 'required|max:100|same:pass1',
            'role' => 'required|in:0,1',
        ]);

        $attributeNames = array(
           'name' => 'Name',
           'email' => 'Email',
           'pass1' => 'Password',
           'pass2' => 'Repeat password',
           'role' => 'Role',
        );

        $validator->setAttributeNames($attributeNames);

        if ($validator->fails()) {
            return redirect('/admin/users/new')
                ->withInput()
                ->withErrors($validator);
        }

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->pass1);
        $user->role = $request->role;

        $file = $request->file('avatar');
        if (isset($file)) {
            $avatar_name = md5(microtime()).'.jpg';
            Image::make($file)->fit(150, 150)->save(public_path('img/avatars/'.$avatar_name))->encode('jpg');
            $user->avatar = $avatar_name;
        } else {
            $user->avatar = '';
        }

        $user->save();

        return redirect('/admin/users')->with('message', 'success|User created successfully.');
    }

    public function getEditUser($id)
    {
        $user = User::find($id);

        return view('admin.users.edit', ['user' => $user]);
    }

    public function saveEditUser($id, Request $request)
    {
        $user = User::find($id);

        $rules = [
            'name' => 'required|max:100',
            'email' => 'required|email|max:100',
            'pass1' => 'min:6|max:100',
            'pass2' => 'max:100|same:pass1',
        ];

        if ($user->role != 2) {
            $rules['role'] = 'required|in:0,1';
            $user->role = $request->role;
        }

        $validator = Validator::make($request->all(), $rules);

        $attributeNames = array(
           'name' => 'Name',
           'email' => 'Email',
           'pass1' => 'Password',
           'pass2' => 'Repeat password',
           'role' => 'Role',
        );

        $validator->setAttributeNames($attributeNames);

        if ($validator->fails()) {
            return redirect('/admin/users/edit/'.$id)
                ->withInput()
                ->withErrors($validator);
        }

        $user->name = $request->name;
        $user->email = $request->email;
        if ($request->pass1 != '') {
            $user->password = Hash::make($request->pass1);
        }

        $file = $request->file('avatar');

        if (isset($request->image_change) && $request->image_change == 1) {
            File::Delete(public_path('img/avatars/'.$user->avatar));
            $user->avatar = '';
        }

        if (isset($file)) {
            File::Delete('img/avatars/'.$user->avatar);
            $avatar_name = md5(microtime()).'.jpg';
            Image::make($file)->fit(150, 150)->save(public_path('img/avatars/'.$avatar_name))->encode('jpg');
            $user->avatar = $avatar_name;
        }

        $user->save();

        return redirect('/admin/users')->with('message', 'success|User saved successfully.');
    }

    public function deleteUser(User $user)
    {
        if ($user->role == 2) {
            return redirect('/admin/users')->with('message', 'error|Unable to delete Super Administrator.');
        }

        File::Delete('img/avatars/'.$user->avatar);
        $user->delete();

        return redirect('/admin/users')->with('message', 'success|User deleted successfully.');
    }
}
