<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => 'web'], function () {

    Route::auth();
    Route::get('/', 'PostController@getAllPosts');
    Route::get('/{id}-{slug}', 'PostController@getPost');
    Route::post('/{id}-{slug}', 'CommentController@saveNewComment');
    Route::get('/user/{id}', 'UserController@getEditUser');
    Route::post('/user/{id}', 'UserController@saveEditUser');

});

Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => ['web','isAdmin']], function () {

    Route::get('/', 'DashboardController@index');

    Route::get('/posts', 'PostController@getListOfPosts');
    Route::get('/posts/edit/{id}', 'PostController@getEditPost');
    Route::post('/posts/edit/{id}', 'PostController@saveEditPost');
    Route::get('/posts/new', 'PostController@getNewPost');
    Route::post('/posts/new', 'PostController@saveNewPost');
    Route::delete('/posts/delete/{post}', 'PostController@deletePost');

    Route::get('/comments', 'CommentController@getListOfComments');
    Route::get('/comments/edit/{id}', 'CommentController@getEditComment');
    Route::post('/comments/edit/{id}', 'CommentController@saveEditComment');
    Route::delete('/comments/delete/{comment}', 'CommentController@deleteComment');

    Route::get('/users', 'UserController@getListOfUsers');
    Route::get('/users/new', 'UserController@getNewUser');
    Route::post('/users/new', 'UserController@saveNewUser');
    Route::get('/users/edit/{id}', 'UserController@getEditUser');
    Route::post('/users/edit/{id}', 'UserController@saveEditUser');
    Route::delete('/users/delete/{user}', 'UserController@deleteUser');

});
