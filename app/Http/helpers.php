<?php

function displayAlert()
{
    if (Session::has('message'))
    {
        list($type, $message) = explode('|', Session::get('message'));
        if($type == 'error')
        {
            $type = 'danger';
            $icon = 'fa-ban';
        }
        if($type == 'success')
        {
            $type = 'success';
            $icon = 'fa-check';
        }
        return '<div class="alert alert-'.$type.' alert-dismissible admin-message-alert">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa '.$icon.'"></i> '.$message.'</h4>
                </div>';
    }
    return '';
}
