<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            'title' => 'Some example post',
            'content' => '<p>Some content.&nbsp;Some content.&nbsp;Some content.&nbsp;Some content.&nbsp;</p><p>[-- READ MORE --]</p><p>Some content.&nbsp;Some content.&nbsp;Some content.&nbsp;Some contentSome content.&nbsp;Some content.&nbsp;Some content.&nbsp;Some contentSome content.&nbsp;Some content.&nbsp;Some content.&nbsp;Some contentSome content.&nbsp;Some content.&nbsp;Some content.&nbsp;Some contentSome content.&nbsp;Some content.&nbsp;Some content.&nbsp;Some contentSome content.&nbsp;Some content.&nbsp;Some content.&nbsp;Some contentSome content.&nbsp;Some content.&nbsp;Some content.&nbsp;Some contentSome content.&nbsp;Some content.&nbsp;Some content.&nbsp;Some contentSome content.&nbsp;Some content.&nbsp;Some content.&nbsp;Some contentSome content.&nbsp;Some content.&nbsp;Some content.&nbsp;Some contentSome content.&nbsp;Some content.&nbsp;Some content.&nbsp;Some contentSome content.&nbsp;Some content.&nbsp;Some content.&nbsp;Some contentSome content.&nbsp;Some content.&nbsp;Some content.&nbsp;Some content.</p>',
            'slug' => 'some-example-post',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'user_id' => 1,
            'cover' => '76769dad57e1123aea3630b0f8387011.jpg',
        ]);
    }
}
