<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comments')->insert([
            'comment' => 'Example comment',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'user_id' => 1,
            'post_id' => 1,
        ]);
    }
}
