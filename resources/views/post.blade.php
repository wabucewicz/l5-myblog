@extends('layouts.app')

@section('content')
            @if($post)
                <!-- Blog Post -->

                <!-- Title -->
                <h1>{{ $post->title }}</h1>

                <!-- Author -->
                <p class="lead">
                    by {{ $post->user->name }}
                </p>

                <hr>

                <!-- Date/Time -->
                <p><span class="glyphicon glyphicon-time"></span> Posted on {{ date('F d Y, h:i', strtotime($post->created_at)) }}</p>

                <hr>
                @if($post->cover != '')
                    <!-- Preview Image -->
                    <img class="img-responsive" src="/img/covers/{{ $post->cover }}" alt="">
                    <hr>
                @endif

                <!-- Post Content -->
                <p class="lead">{{ strip_tags($post->lead) }}</p>
                <p>{!! $post->content !!}</p>

                <hr>

                <!-- Blog Comments -->

                @if (Auth::guest())
                    <a href="{{ url('/login') }}">Login</a> or
                    <a href="{{ url('/register') }}">Register</a> to add your comments.
                @else
                    @if (count($errors) > 0)
                        <!-- Form Error List -->
                        <div class="alert alert-danger admin-message-alert">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Whoops! Something went wrong!</h4>
                            <br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <!-- Comments Form -->
                    <div class="well">
                        <h4>Leave a Comment:</h4>
                        <form role="form" action="{{ url('/'.$post->id.'-'.$post->slug) }}" method="post">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <textarea class="form-control" rows="3" maxlength="1000" style="resize:vertical;" name="comment">{!! old('comment', '') !!}</textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                @endif

                <hr>

                <!-- Posted Comments -->

                @foreach($comments as $comment)
                <!-- Comment -->
                    <div class="media">
                        <div class="pull-left">
                            @if($comment->user->avatar != '')
                                <img class="img-circle" src="/img/avatars/{{ $comment->user->avatar }}" alt="">
                            @else
                                <img class="img-circle" src="/img/avatars/default-avatar.jpg" alt="">
                            @endif
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">{{ $comment->user->name }}
                                <small>{{ date('F d Y, h:i:s', strtotime($comment->created_at)) }}</small>
                            </h4>
                            {{ $comment->comment }}
                        </div>
                    </div>
                @endforeach


            @else
                <div style="text-align: center;">
                    Post doesn't exist
                </div>
            @endif
@endsection

<style media="screen">
    .img-circle {
        border-radius: 50%;
        z-index: 5;
        height: 64px;
        width: 64px;
    }
</style>
