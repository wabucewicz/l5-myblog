@extends('admin.layouts.admin')


@section('header')
    <h1>
        Edit user
        <small>Edit existing user</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="/admin/users"><i class="fa fa-users"></i> Users</a></li>
        <li class="active">Edit</li>
    </ol>
@endsection


@section('content')
    <div class="row">
        <div class="col-xs-12">
            <form class="form-horizontal col-md-10 col-lg-6" role="form" action="{{ url('/admin/users/edit/'.$user->id) }}" method="post" enctype="multipart/form-data">

                {!! csrf_field() !!}

                <div class="form-group">
                    <label for="name" class="col-sm-4 control-label" style="text-align:left;">Name</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $user->name) }}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="col-sm-4 control-label" style="text-align:left;">Email</label>
                    <div class="col-sm-8">
                        <input type="email" class="form-control" id="email" name="email" value="{{ old('email', $user->email) }}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="pass1" class="col-sm-4 control-label" style="text-align:left;">Password</label>
                    <div class="col-sm-8">
                        <input type="password" class="form-control" id="pass1" name="pass1" value="">
                    </div>
                </div>

                <div class="form-group">
                    <label for="pass2" class="col-sm-4 control-label" style="text-align:left;">Repeat password</label>
                    <div class="col-sm-8">
                        <input type="password" class="form-control" id="pass2" name="pass2" value="">
                    </div>
                </div>

                <div class="form-group">
                    <label for="role" class="col-sm-4 control-label" style="text-align:left;">Role</label>
                    <div class="col-sm-8">
                        <select class="form-control" name="role" id="role" {{ $user->role == 2 ? 'disabled' : '' }}>
                            @if($user->role == 2)
                                <option value="2" selected>
                                    Super Administrator
                                </option>
                            @else
                                <option value="0" {{ old('role', $user->role) == 0 ? 'selected' : '' }}>
                                    Standard user
                                </option>
                                <option value="1" {{ old('role', $user->role) == 1 ? 'selected' : '' }}>
                                    Administrator
                                </option>
                            @endif
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="avatar" class="col-sm-4 control-label" style="text-align:left;">Avatar image</label>
                    <div class="col-sm-8">
                        @if($user->avatar != '')
                            <img src="/img/avatars/{{ $user->avatar }}" alt="" /><br>
                            <div class="radio">
                                <label><input type="radio" id="image_del" name="image_change" value="1">Delete image</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" id="image_change" name="image_change" checked="checked" value="2">Keep existing image or load new one</label>
                            </div>
                            <br>

                        @endif
                        <input type="file" id="avatar" name="avatar">
                        <p class="help-block">Your image will be fit to 150px x 150px and saved as JPG</p>
                    </div>
                </div>

                <a href="/admin/users"><button type="button" class="btn btn btn-warning"><i class="fa fa-chevron-left"></i> Cancel</button></a>
                <button type="submit" class="btn btn btn-success"><i class="fa fa-check"></i> Save user</button>
            </form>
        </div>
    </div>
@endsection

@section('assets-bottom')
    @if($user->avatar != '')
    <script type="text/javascript">
        $('#image_del').click(function()
        {
            $('#avatar').attr("disabled","disabled");
        });

        $('#image_change').click(function()
        {
            $('#avatar').removeAttr("disabled");
        });
    </script>
    @endif
@endsection
