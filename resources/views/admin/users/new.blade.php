@extends('admin.layouts.admin')


@section('header')
    <h1>
        New user
        <small>Create new blog's user</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="/admin/users"><i class="fa fa-users"></i> Users</a></li>
        <li class="active">New</li>
    </ol>
@endsection


@section('content')
    <div class="row">
        <div class="col-xs-12">
            <form class="form-horizontal col-md-10 col-lg-6" role="form" action="{{ url('/admin/users/new') }}" method="post" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <div class="form-group">
                    <label for="name" class="col-sm-4 control-label" style="text-align:left;">Name</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-4 control-label" style="text-align:left;">Email</label>
                    <div class="col-sm-8">
                        <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="pass1" class="col-sm-4 control-label" style="text-align:left;">Password</label>
                    <div class="col-sm-8">
                        <input type="password" class="form-control" id="pass1" name="pass1" value="{{ old('pass1') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="pass2" class="col-sm-4 control-label" style="text-align:left;">Repeat password</label>
                    <div class="col-sm-8">
                        <input type="password" class="form-control" id="pass2" name="pass2" value="{{ old('pass2') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="role" class="col-sm-4 control-label" style="text-align:left;">Role</label>
                    <div class="col-sm-8">
                        <select class="form-control" name="role" id="role">
                            <option value="0" {{ old('role') == 0 ? 'checked' : '' }}>Standard user</option>
                            <option value="1" {{ old('role') == 1 ? 'checked' : '' }}>Administrator</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="avatar" class="col-sm-4 control-label" style="text-align:left;">Avatar image</label>
                    <div class="col-sm-8">
                        <input type="file" id="avatar" name="avatar">
                        <p class="help-block">Your image will be fit to 150px x 150px and saved as JPG</p>
                    </div>
                </div>
                <a href="/admin/users"><button type="button" class="btn btn btn-warning"><i class="fa fa-chevron-left"></i> Cancel</button></a>
                <button type="submit" class="btn btn btn-success"><i class="fa fa-check"></i> Create user</button>
            </form>
        </div>
    </div>
@endsection
