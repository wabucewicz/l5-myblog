@extends('admin.layouts.admin')


@section('assets-top')
    <link rel="stylesheet" href="/css/admin/AdminLTE/dataTables.bootstrap.css">
@endsection


@section('header')
    <h1>
        List of users
        <small>List of all blog's users</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Users</li>
    </ol>
@endsection


@section('content')
    <div class="box">
        <div class="box-body">
            <table id="table-list" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Role</th>
                        <th>Created at</th>
                        <th style="width:12%;"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td><a href="{{ url('/admin/users/edit/'.$user->id) }}">{{ $user->name }}</a></td>
                        <td>
                            @if($user->role == 0)
                                Standard user
                            @elseif($user->role == 1)
                                Administrator
                            @elseif($user->role == 2)
                                Super Administrator
                            @endif
                        </td>
                        <td>{{ $user->created_at }}</td>
                        <td style="text-align:right;">
                            <form action="{{ url('/admin/users/delete/'.$user->id) }}" method="POST">
                                {!! csrf_field() !!}
                                {!! method_field('DELETE') !!}
                                <a href="/admin/users/edit/{{ $user->id }}">
                                    <button type="button" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></button>
                                </a>
                                @if($user->role != 2)
                                    <button type="submit" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></button>
                                @endif
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
    <div class="row">
        <div class="col-xs-12">
            <a href="/admin/users/new"><button type="button" class="btn btn btn-success"><i class="fa fa-plus"></i> Create user</button></a>
        </div>
    </div>
@endsection


@section('assets-bottom')
    <script src="/js/admin/AdminLTE/jquery.dataTables.min.js"></script>
    <script src="/js/admin/AdminLTE/dataTables.bootstrap.min.js"></script>
    <script src="/js/admin/AdminLTE/jquery.slimscroll.min.js"></script>
    <script src="/js/admin/AdminLTE/fastclick.min.js"></script>
    <script>
    $(function () {
        $('#table-list').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            "columnDefs": [ {
                "targets": 3,
                "orderable": false
            } ]

        });
    });
    </script>
@endsection
