@extends('admin.layouts.admin')


@section('assets-top')
    <link rel="stylesheet" href="/css/admin/AdminLTE/dataTables.bootstrap.css">
@endsection


@section('header')
    <h1>
        List of comments
        <small>List of all post's comments</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Comments</li>
    </ol>
@endsection


@section('content')
    <div class="box">
        @if(count($comments)>0)
        <div class="box-body">
            <table id="table-list" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Author</th>
                        <th>Comment</th>
                        <th>Post</th>
                        <th>Created at</th>
                        <th style="width:12%;"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($comments as $comment)
                    <tr>
                        <td><a href="{{ url('/admin/users/edit/'.$comment->user_id) }}">{{ $comment->user->name }}</a></td>
                        <td><a href="{{ url('/admin/comments/edit/'.$comment->id) }}">{{ $comment->comment }}</a></td>
                        <td><a href="{{ url('/admin/posts/edit/'.$comment->post_id) }}">{{ $comment->post->title }}</a></td>
                        <td>{{ $comment->created_at }}</td>
                        <td style="text-align:right;">
                            <form action="{{ url('/admin/comments/delete/'.$comment->id) }}" method="POST">
                                {!! csrf_field() !!}
                                {!! method_field('DELETE') !!}
                                <a href="/admin/comments/edit/{{ $comment->id }}">
                                    <button type="button" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></button>
                                </a>
                                <button type="submit" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div><!-- /.box-body -->
        @else
        No comments
        @endif
    </div><!-- /.box -->
@endsection

@section('assets-bottom')
    <script src="/js/admin/AdminLTE/jquery.dataTables.min.js"></script>
    <script src="/js/admin/AdminLTE/dataTables.bootstrap.min.js"></script>
    <script src="/js/admin/AdminLTE/jquery.slimscroll.min.js"></script>
    <script src="/js/admin/AdminLTE/fastclick.min.js"></script>
    <script>
    $(function () {
        $('#table-list').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            "columnDefs": [ {
                "targets": 4,
                "orderable": false
            } ]

        });
    });
    </script>
@endsection
