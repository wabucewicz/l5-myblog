@extends('admin.layouts.admin')


@section('header')
    <h1>
        Edit comment
        <small>Modify post's comment</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="/admin/comments"><i class="fa fa-comments"></i> Comments</a></li>
        <li class="active">Edit</li>
    </ol>
@endsection


@section('content')
    <div class="row">
        <div class="col-xs-12">
            @if($comment)
            <form role="form" action="{{ url('/admin/comments/edit/'.$comment->id) }}" method="post">
                {!! csrf_field() !!}
                <div class="form-group">
                    <label>Comment</label>
                    <textarea name="comment" class="form-control">{!! old('comment', $comment->comment) !!}</textarea>
                </div>
                <a href="/admin/comments"><button type="button" class="btn btn btn-warning"><i class="fa fa-chevron-left"></i> Cancel</button></a>
                <button type="submit" class="btn btn btn-success"><i class="fa fa-check"></i> Save comment</button>
            </form>
            @else
                Comment doesn't exist
            @endif
        </div>
    </div>
@endsection
