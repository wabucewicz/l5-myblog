@extends('admin.layouts.admin')


@section('header')
    <h1>
        Edit post
        <small>Modify existing post</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="/admin/posts"><i class="fa fa-pencil-square-o"></i> Posts</a></li>
        <li class="active">Edit</li>
    </ol>
@endsection


@section('content')
    <div class="row">
        <div class="col-xs-12">
            @if($post)
            <form role="form" action="{{ url('/admin/posts/edit/'.$post->id) }}" method="post" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <div class="form-group">
                    <label>Title</label>
                    <input id="title" class="form-control" name="title" value="{{ old('title', $post->title) }}">
                </div>
                <br>
                <div class="form-group">
                    <label>URL slug</label>
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon3">{{URL::to('/')}}/{{$post->id}}-</span>
                        <input id="slug" type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3" name="slug" value="{{ old('slug', $post->slug) }}">
                    </div>
                </div>
                <br>
                <div class="form-group">
                    <label>Cover image</label>
                    <div class="input-group">
                        @if($post->cover != '')
                            <img src="/img/covers/{{ $post->cover }}" alt="" style="max-width:100%"/><br>
                            <div class="radio">
                                <label><input type="radio" id="image_del" name="image_change" value="1">Delete image</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" id="image_change" name="image_change" checked="checked" value="2">Keep existing image or load new one</label>
                            </div>
                        @endif
                        <input type="file" id="cover" name="cover">
                        <p class="help-block">Your image will be fit to 900px x 300px and saved as JPG</p>
                    </div>
                </div>
                <br>
                <div class="form-group">
                    <label>Post content</label>
                    <textarea name="content" class="form-control">
                        {!! old('content', $post->content) !!}
                    </textarea>
                </div>
                <a href="/admin/posts"><button type="button" class="btn btn btn-warning"><i class="fa fa-chevron-left"></i> Cancel</button></a>
                <button type="submit" class="btn btn btn-success"><i class="fa fa-check"></i> Save post</button>
            </form>
            @else
                Post doesn't exist
            @endif
        </div>
    </div>
@endsection


@section('assets-bottom')
    <script type="text/javascript" src="{{ asset('/js/admin/tinymce/tinymce.min.js') }}"></script>
    <script type="text/javascript">
    tinymce.init({
        content_css : "/css/admin/TinyMCE/tinymce_style.css",
        selector : "textarea",
        plugins : ["advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "insertdatetime media table contextmenu paste", "autoresize"],
        toolbar : "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | read-more-button",
        setup: function (editor) {
            editor.addButton('read-more-button', {
                text: 'Read More tag',
                icon: false,
                onclick: function () {
                    editor.insertContent('<p>[-- READ MORE --]</p>');
                }
            });
        },
    });

    function slugify(text)
    {
      return text.toString().toLowerCase()
        .replace(/\s+/g, '-')           // Replace spaces with -
        .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
        .replace(/\-\-+/g, '-')         // Replace multiple - with single -
        .replace(/^-+/, '')             // Trim - from start of text
        .replace(/-+$/, '');            // Trim - from end of text
    }

    $(document).ready(function(){
        $("#title").on('input', function(){
            $("#slug").val(slugify($("#title").val()));
        });
    });

    @if($post->cover != '')
        $('#image_del').click(function()
        {
            $('#cover').attr("disabled","disabled");
        });

        $('#image_change').click(function()
        {
            $('#cover').removeAttr("disabled");
        });

    @endif
    </script>
@endsection
