@extends('admin.layouts.admin')


@section('assets-top')
    <link rel="stylesheet" href="/css/admin/AdminLTE/dataTables.bootstrap.css">
@endsection


@section('header')
    <h1>
        List of posts
        <small>List of all blog's posts</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Posts</li>
    </ol>
@endsection


@section('content')
    <div class="box">
        @if(count($posts)>0)
        <div class="box-body">
            <table id="table-list" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th style="width:5%">ID</th>
                        <th>Title</th>
                        <th class="hidden-sm hidden-xs" style="width:20%">Created at</th>
                        <th class="hidden-sm hidden-xs" style="width:20%">Updated at</th>
                        <th class="hidden-sm hidden-xs" style="width:10%">Author</th>
                        <th style="width:12%;"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($posts as $post)
                    <tr>
                        <td>{{ $post->id }}</td>
                        <td><a href="/admin/posts/edit/{{ $post->id }}">{{ $post->title }}</a></td>
                        <td class="hidden-sm hidden-xs">{{ $post->created_at }}</td>
                        <td class="hidden-sm hidden-xs">{{ $post->updated_at }}</td>
                        <td class="hidden-sm hidden-xs">{{ $post->user->name }}</td>
                        <td style="text-align:right;">
                            <form action="{{ url('/admin/posts/delete/'.$post->id) }}" method="POST">
                                {!! csrf_field() !!}
                                {!! method_field('DELETE') !!}
                                <a href="/admin/posts/edit/{{ $post->id }}">
                                    <button type="button" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></button>
                                </a>
                                <button type="submit" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div><!-- /.box-body -->
        @else
        No posts
        @endif
    </div><!-- /.box -->

    <div class="row">
        <div class="col-xs-12">
            <a href="/admin/posts/new"><button type="button" class="btn btn btn-success"><i class="fa fa-plus"></i> Create post</button></a>
        </div>
    </div>
@endsection


@section('assets-bottom')
    <script src="/js/admin/AdminLTE/jquery.dataTables.min.js"></script>
    <script src="/js/admin/AdminLTE/dataTables.bootstrap.min.js"></script>
    <script src="/js/admin/AdminLTE/jquery.slimscroll.min.js"></script>
    <script src="/js/admin/AdminLTE/fastclick.min.js"></script>
    <script>
    $(function () {
        $('#table-list').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            "columnDefs": [ {
                "targets": 5,
                "orderable": false
            } ]

        });
    });
    </script>
@endsection
