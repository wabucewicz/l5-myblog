@extends('admin.layouts.admin')


@section('header')
    <h1>
        Dashboard
        <small>Your home panel site</small>
    </h1>
    <ol class="breadcrumb">
        <li class="active">Dashboard</li>
    </ol>
@endsection


@section('content')

    <div class="row">

        <div class="col-md-4">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="ion ion-compose"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Posts</span>
                    <span class="info-box-number">{{ $count['posts'] }}</span>
                </div>
            <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>

        <div class="col-md-4">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="ion ion-chatbubbles"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Comments</span>
                    <span class="info-box-number">{{ $count['comments'] }}</span>
                </div>
            <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>

        <div class="col-md-4">
            <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Users</span>
                    <span class="info-box-number">{{ $count['users'] }}</span>
                </div>
            <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>

    </div>

    <div class="row">

        <div class="col-lg-4">
            <div class="box">
                <div class="box-body">
                    <h4 class="box-title">Recent posts</h4>
                    @if($count['posts']>0)
                        <ul class="dashboard-ul">
                            @foreach($posts as $post)
                                <li><a href="{{ action('Admin\PostController@getEditPost', [$post->id]) }}">{{ $post->title }}</a></li>
                            @endforeach
                        </ul>
                    @else
                        No posts
                    @endif
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="box">
                <div class="box-body">
                    <h4 class="box-title">Recent comments</h4>
                    @if($count['comments']>0)
                        <ul class="dashboard-ul">
                            @foreach($comments as $comment)
                                <li><a href="{{ action('Admin\CommentController@getEditComment', [$comment->id]) }}">{{ $comment->comment }}</a></li>
                            @endforeach
                        </ul>
                    @else
                        No comments
                    @endif
                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="box">
                <div class="box-body">
                    <h4 class="box-title">Recent registered users</h4>
                    <ul class="dashboard-ul">
                        @foreach($users as $user)
                            <li>
                                <a href="{{ action('Admin\UserController@getEditUser', [$user->id]) }}">{{ $user->name }}</a>

                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>

    </div>

@endsection
