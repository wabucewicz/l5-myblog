@extends('layouts.app')


@section('content')
<div class="panel panel-default">
    <div class="panel-heading">User profile</div>
    <div class="panel-body">
        {!! displayAlert() !!}
        @if (count($errors) > 0)
            <!-- Form Error List -->
            <div class="alert alert-danger admin-message-alert">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-ban"></i> Whoops! Something went wrong!</h4>
                <br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form class="form-horizontal" role="form" action="{{ url('/user/'.$user->id) }}" method="post" enctype="multipart/form-data">

            {!! csrf_field() !!}

            <div class="form-group">
                <label for="name" class="col-sm-3 control-label" style="text-align:left;">Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $user->name) }}">
                </div>
            </div>

            <div class="form-group">
                <label for="email" class="col-sm-3 control-label" style="text-align:left;">Email</label>
                <div class="col-sm-9">
                    <input type="email" class="form-control" id="email" name="email" value="{{ old('email', $user->email) }}">
                </div>
            </div>

            <div class="form-group">
                <label for="pass1" class="col-sm-3 control-label" style="text-align:left;">Password</label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" id="pass1" name="pass1" value="">
                </div>
            </div>

            <div class="form-group">
                <label for="pass2" class="col-sm-3 control-label" style="text-align:left;">Repeat password</label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" id="pass2" name="pass2" value="">
                </div>
            </div>

            <div class="form-group">
                <label for="avatar" class="col-sm-3 control-label" style="text-align:left;">Avatar image</label>
                <div class="col-sm-9">
                    @if($user->avatar != '')
                        <img src="/img/avatars/{{ $user->avatar }}" alt="" /><br>
                        <div class="radio">
                            <label><input type="radio" id="image_del" name="image_change" value="1">Delete image</label>
                        </div>
                        <div class="radio">
                            <label><input type="radio" id="image_change" name="image_change" checked="checked" value="2">Keep existing image or load new one</label>
                        </div>
                        <br>

                    @endif
                    <input type="file" id="avatar" name="avatar">
                    <p class="help-block">Your image will be fit to 150px x 150px and saved as JPG</p>
                </div>
            </div>

            <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save profile data</button>
        </form>
    </div>
</div>
@endsection

@section('assets-bottom')
    @if($user->avatar != '')
    <script type="text/javascript">
        $('#image_del').click(function()
        {
            $('#avatar').attr("disabled","disabled");
        });

        $('#image_change').click(function()
        {
            $('#avatar').removeAttr("disabled");
        });
    </script>
    @endif
@endsection
