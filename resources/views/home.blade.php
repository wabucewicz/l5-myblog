@extends('layouts.app')

@section('content')

            @if(count($posts) > 0)
                @foreach($posts as $post)

                    <!-- First Blog Post -->
                    <h2>
                        <a href="/{{ $post->id }}-{{ $post->slug }}">{{ $post->title }}</a>
                    </h2>
                    <p class="lead">
                        by {{ $post->user->name }}
                    </p>
                    <p><span class="glyphicon glyphicon-time"></span> Posted on {{ date('F d Y, h:i', strtotime($post->created_at)) }}</p>
                    @if($post->cover != '')
                        <hr>
                        <a href="/{{ $post->id }}-{{ $post->slug }}"><img class="img-responsive" src="/img/covers/{{ $post->cover }}" alt=""></a>
                    @endif
                    <hr>
                    <p>{!! $post->lead !!}</p>
                    <a class="btn btn-primary" href="/{{ $post->id }}-{{ $post->slug }}">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>

                    <hr style="height:1px;background-color:#C5C5C5;margin-top:30px;margin-bottom:30px;">

                @endforeach
                {!! $posts->links() !!}
            @else
                <div style="text-align: center;">
                    No posts
                </div>
            @endif
@endsection
